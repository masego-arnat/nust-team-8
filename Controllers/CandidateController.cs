﻿using AspNetCoreHero.ToastNotification.Abstractions;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nust_Vote_System.Datalayer;
using Nust_Vote_System.DatatabaseModel;
using Nust_Vote_System.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Nust_Vote_System.Controllers
{
    public class CandidateController : Controller
    {
        private readonly VotingContext applicationDbContext;
        private readonly IMapper mapper;
        private readonly ILogger<AccountController> logger;
        private readonly INotyfService _notyf;
        private IWebHostEnvironment _Environment;

        public CandidateController(IWebHostEnvironment Environment, INotyfService notyf, VotingContext applicationDbContext, IMapper mapper, ILogger<AccountController> logger)
        {
            this.applicationDbContext = applicationDbContext;
            this.mapper = mapper;
            this.logger = logger;
            _notyf = notyf;

            _Environment = Environment;
        }

        [HttpGet]
        public IActionResult Index()
        {

            CadidatesViewModel movieView = new CadidatesViewModel();
            List<CadidatesViewModel> model = new List<CadidatesViewModel>();
            var rates = applicationDbContext.Candidates.ToList();

            var ratesModel = mapper.Map<IEnumerable<CadidatesViewModel>>(rates);

            CadidatesViewModelList detailsViewModel = new CadidatesViewModelList();

            detailsViewModel.Cadidates = new List<CadidatesViewModel>();

            detailsViewModel.Cadidates = (List<CadidatesViewModel>)ratesModel;

            return View(detailsViewModel);

        }


        public IActionResult Edit(int id)
        {


            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }

                var rates = applicationDbContext.Candidates.FindAsync(id).Result;

                var ratesmodel = mapper.Map<CadidatesViewModel>(rates);
                return View(ratesmodel);
            }
            catch (Exception e)
            {
                logger.LogError(e.Message);
                return BadRequest();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Obsolete]
        public async Task<IActionResult> Edit(int id, CadidatesViewModel model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            _notyf.Success("The movie has been Edited successfully..");
            Candidates userEntity = new Candidates();

            if (ModelState.IsValid)
            {
                try
                {
                    if (model.ImageFile != null)
                    {


                        string wwwRootPath = _Environment.WebRootPath;
                        string fileName = Path.GetFileNameWithoutExtension(model.ImageFile.FileName);
                        string extension = Path.GetExtension(model.ImageFile.FileName);
                        model.ImageName = fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                        string path = Path.Combine(wwwRootPath + "/Image/", fileName);

                        using (var fileStream = new FileStream(path, FileMode.Create))
                        {
                            await model.ImageFile.CopyToAsync(fileStream);
                        }

                        var CandidatesModel = mapper.Map<Candidates>(model);


                        applicationDbContext.Candidates.Update(CandidatesModel);
                        await applicationDbContext.SaveChangesAsync();
                    }
                    if (model.ImageFile == null)
                    {

                        var rates = applicationDbContext.Candidates.FindAsync(id).Result;

                        model.ImageFile = rates.ImageFile;
                        var CandidatesModel = mapper.Map<Candidates>(model);

                        applicationDbContext.Candidates.Update(CandidatesModel);
                        await applicationDbContext.SaveChangesAsync();
                    }

                }
                catch (Exception x)
                {

                }
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }









        public IActionResult Delete(int id)
        {
            var dispatch = new Candidates();

            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }

                dispatch = applicationDbContext.Candidates.FindAsync(id).Result;
                return View(dispatch);
            }
            catch (Exception e)
            {
                logger.LogError(e.Message);
                return BadRequest();
            }

        }

        [HttpPost]
        public async Task<IActionResult> DeleteConfirm(int id)
        {
            var dispatch = new Candidates();

            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }

                dispatch = applicationDbContext.Candidates.FindAsync(id).Result;
                applicationDbContext.Candidates.Remove(dispatch);
                await applicationDbContext.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                logger.LogError(e.Message);
                return BadRequest();
            }

        }






        public IActionResult GetAllCandidates()
        {

            try
            {
                var cadidates = applicationDbContext.Candidates.ToList();

                var ratesModel = mapper.Map<IEnumerable<CadidatesViewModel>>(cadidates);


                return Json(ratesModel);
            }
            catch (Exception e)
            {
                //logger.LogError(e.Message);
                return BadRequest();
            }
        }


        [HttpGet]
        public IActionResult AddCandidate()
        {
            CadidatesViewModel movieDetailsViewModel = new CadidatesViewModel();
            _notyf.Information("You are about to ADD a Candidate ", 5);
            return View(movieDetailsViewModel);
        }

        [HttpPost]
        [Obsolete]
        public async Task<IActionResult> AddCandidateAsync(CadidatesViewModel model)
        {
            //Candidates userEntity = new Candidates();

            _notyf.Success("A Candidate was added ", 5);
            if (ModelState.IsValid)
            {
                //Save image to wwwroot/image
                string wwwRootPath = _Environment.WebRootPath;
                string fileName = Path.GetFileNameWithoutExtension(model.ImageFile.FileName);
                string extension = Path.GetExtension(model.ImageFile.FileName);
                model.ImageName = fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                string path = Path.Combine(wwwRootPath + "/Image/", fileName);
                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await model.ImageFile.CopyToAsync(fileStream);
                }
                var cadidates = applicationDbContext.Candidates.ToList();

                var CandidatesModel = mapper.Map<Candidates>(model);

                await applicationDbContext.Candidates.AddAsync(CandidatesModel);
                await applicationDbContext.SaveChangesAsync();

                //this is to send data tp the char Js FUNTION/METHOND 
                //await _movieHub.Clients.All.SendAsync("Create", Analytics());

                if (CandidatesModel.Id > 0)
                {
                    return RedirectToAction("index");

                }

            }

            return View(model);

        }


    }
}
