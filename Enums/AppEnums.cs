﻿namespace Nust_Vote_System.Enums
{
    public class AppEnums
    {
        public static class Positions
        {
            public enum CandidatePositions
            {
                President,
                Vice_President,
                Health_Development,
                Secretary_General,
                Internal_Affairs,
                Finance,
                Information_Publicity,
                Academic_Affairs,
                Recreation,
                Accommodation,
                External_Affairs,
            }
        }
    }
}