﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nust_Vote_System.DatatabaseModel
{
    public class StaffMember
    {
        [Key]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public string City { get; set; }
        public string PhoneNumber { get; set; }
        [NotMapped]
        public string Password { get; set; }
    }

}


