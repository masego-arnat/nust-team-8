﻿using System.ComponentModel.DataAnnotations;

namespace Nust_Vote_System.DatatabaseModel
{
    public class Voters
    {
        [Key]
        public int Id { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }
        public int StudentNumber { get; set; }
        public string StudyMode { get; set; }

    }
}
