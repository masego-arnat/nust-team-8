﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nust_Vote_System.DatatabaseModel
{
    public class Candidates
    {
        [Key]
        public int Id { get; set; }
        public string FullName { get; set; }
        public int StudentNumber { get; set; }
        public string StudyMode { get; set; }
        public string Position { get; set; }
        public string ImageName { get; set; }
        [NotMapped]
        public IFormFile ImageFile { get; set; }

    }
}
