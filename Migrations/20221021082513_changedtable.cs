﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Nust_Vote_System.Migrations
{
    public partial class changedtable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Fname",
                table: "StaffMember");

            migrationBuilder.DropColumn(
                name: "Fname",
                table: "Candidates");

            migrationBuilder.RenameColumn(
                name: "Lname",
                table: "StaffMember",
                newName: "FullName");

            migrationBuilder.RenameColumn(
                name: "Lname",
                table: "Candidates",
                newName: "FullName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FullName",
                table: "StaffMember",
                newName: "Lname");

            migrationBuilder.RenameColumn(
                name: "FullName",
                table: "Candidates",
                newName: "Lname");

            migrationBuilder.AddColumn<string>(
                name: "Fname",
                table: "StaffMember",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Fname",
                table: "Candidates",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
