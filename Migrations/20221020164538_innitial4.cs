﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Nust_Vote_System.Migrations
{
    public partial class innitial4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "StudentNum",
                table: "Student",
                newName: "StudentNumber");

            migrationBuilder.AddColumn<string>(
                name: "StudentName",
                table: "Student",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StaffName",
                table: "StaffMemeber",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StudentName",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "StaffName",
                table: "StaffMemeber");

            migrationBuilder.RenameColumn(
                name: "StudentNumber",
                table: "Student",
                newName: "StudentNum");
        }
    }
}
